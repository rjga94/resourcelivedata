package com.gitlab.rjga94.resourcelivedata

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.gitlab.rjga94.resourcelivedata.models.Resource

abstract class PagedCachedResource<ResultType>
@MainThread constructor() {

    private val result = MediatorLiveData<Resource<ResultType>>()
    private var page = 1
    private var loading = false

    init {
        result.value = Resource.loading(null)
        getData()
        loadNextPageTrigger()?.observeForever {
            getData()
        }
    }

    private fun getData() {
        if (loading) return else loading = true
        @Suppress("LeakingThis")
        val dbSource = loadFromDb(page)
        result.addSource(dbSource) { data ->
            result.removeSource(dbSource)
            setValue(Resource.success(data))
            page += 1
            loading = false
        }
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    protected open fun onFetchFailed() {}

    fun asLiveData() = result as LiveData<Resource<ResultType>>

    @MainThread
    protected open fun loadNextPageTrigger(): LiveData<Boolean>? = null

    @MainThread
    protected abstract fun loadFromDb(page: Int): LiveData<ResultType>
}