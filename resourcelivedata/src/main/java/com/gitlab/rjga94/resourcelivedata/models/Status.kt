package com.gitlab.rjga94.resourcelivedata.models

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}