package com.gitlab.rjga94.resourcelivedata

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.gitlab.rjga94.resourcelivedata.models.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.ResponseBody

abstract class PagedNetworkResource<ResultType>
@MainThread constructor() {

    private val result = MediatorLiveData<Resource<ResultType>>()
    private var page = 1
    private var loading = false

    init {
        result.value = Resource.loading(null)
        fetchFromNetwork()
        nextPageTrigger()?.observeForever {
            fetchFromNetwork()
        }
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork() {
        if (loading) return else loading = true
        val apiResponse = createCall(page)
        result.addSource(apiResponse) { response ->
            result.removeSource(apiResponse)
            when (response) {
                is ApiSuccessResponse -> {
                    CoroutineScope(Dispatchers.IO).launch {
                        val processed = processResponse(response)
                        page = if (response.nextPage != null) response.nextPage!! else page + 1
                        CoroutineScope(Dispatchers.Main).launch {
                            setValue(Resource.success(processed))
                        }
                        loading = false
                    }
                }
                is ApiEmptyResponse -> {
                    CoroutineScope(Dispatchers.Main).launch {
                        setValue(Resource.success(null))
                    }
                    loading = false
                }
                is ApiErrorResponse -> {
                    onFetchFailed()
                    setValue(Resource.error(response.errorMessage, null))
                    loading = false
                }
            }
        }
    }

    protected open fun onFetchFailed() {}

    fun asLiveData() = result as LiveData<Resource<ResultType>>

    @MainThread
    protected abstract fun nextPageTrigger(): LiveData<Boolean>?

    @MainThread
    protected abstract fun createCall(page: Int): LiveData<ApiResponse<ResponseBody>>

    @WorkerThread
    protected abstract fun processResponse(response: ApiSuccessResponse<ResponseBody>): ResultType
}