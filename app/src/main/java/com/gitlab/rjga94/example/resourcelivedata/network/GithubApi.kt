package com.gitlab.rjga94.example.resourcelivedata.network

import androidx.lifecycle.LiveData
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import com.gitlab.rjga94.resourcelivedata.models.ApiResponse

interface GithubApi {

    @GET("users/{user}/repos")
    fun getRepos(@Path("user") user: String): LiveData<ApiResponse<ResponseBody>>

    @GET("users/{user}/repos")
    fun getRepos(@Path("user") user: String, @Query("page") page: Int): LiveData<ApiResponse<ResponseBody>>
}