package com.gitlab.rjga94.example.resourcelivedata.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.gitlab.rjga94.example.resourcelivedata.data.daos.RepoDao
import com.gitlab.rjga94.example.resourcelivedata.data.entities.RepoPage
import com.gitlab.rjga94.example.resourcelivedata.utilities.DatabaseConverters

@Database(entities = [RepoPage::class], version = 1, exportSchema = false)
@TypeConverters(DatabaseConverters::class)
abstract class AppDatabase: RoomDatabase() {
    abstract fun repoDao(): RepoDao

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        // Create and pre-populate the database. See this article for more details:
        // https://medium.com/google-developers/7-pro-tips-for-room-fbadea4bfbd1#4785
        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, "app-db").build()
        }
    }
}