package com.gitlab.rjga94.example.resourcelivedata.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gitlab.rjga94.example.resourcelivedata.data.daos.RepoDao
import com.gitlab.rjga94.example.resourcelivedata.data.entities.RepoPage
import com.gitlab.rjga94.example.resourcelivedata.data.models.Repo
import com.gitlab.rjga94.resourcelivedata.NetworkResource
import com.gitlab.rjga94.resourcelivedata.PagedCachedResource
import com.gitlab.rjga94.resourcelivedata.PagedNetworkCachedResource
import com.gitlab.rjga94.resourcelivedata.PagedNetworkResource
import okhttp3.ResponseBody
import com.gitlab.rjga94.example.resourcelivedata.network.AppService
import com.gitlab.rjga94.resourcelivedata.models.ApiResponse
import com.gitlab.rjga94.resourcelivedata.models.ApiSuccessResponse

class RepoRepository(
    private val appService: AppService,
    private val repoDao: RepoDao
) {

    private val nextPageTrigger = MutableLiveData<Boolean>()

    fun triggerLoadNextPage() {
        nextPageTrigger.postValue(true)
    }

//    fun getCachedOnly() = object : CachedResource<List<Repo>>() {
//        override fun loadFromDb(): LiveData<List<Repo>> {
//
//        }
//    }.asLiveData()

    fun getPagedCachedOnly() = object : PagedCachedResource<RepoPage>() {
        override fun loadNextPageTrigger(): LiveData<Boolean>? {
            return nextPageTrigger
        }

        override fun loadFromDb(page: Int): LiveData<RepoPage> {
            return repoDao.load(page)
        }
    }.asLiveData()

    fun getNetworkOnly(user: String) = object : NetworkResource<List<Repo>>() {
        override fun createCall(): LiveData<ApiResponse<ResponseBody>> {
            return appService.githubApi.getRepos(user)
        }

        override fun processResponse(response: ApiSuccessResponse<ResponseBody>): List<Repo> {
            return Repo.parseResponseBody(response.body)
        }
    }.asLiveData()

    fun getPagedNetworkOnly(user: String) = object : PagedNetworkResource<RepoPage>() {
        override fun nextPageTrigger(): LiveData<Boolean>? {
            return nextPageTrigger
        }
        override fun createCall(page: Int): LiveData<ApiResponse<ResponseBody>> {
            return appService.githubApi.getRepos(user, page)
        }

        override fun processResponse(response: ApiSuccessResponse<ResponseBody>): RepoPage {
            return RepoPage(
                response.currentPage,
                Repo.parseResponseBody(response.body)
            )
        }
    }.asLiveData()

    fun getPaged() = object : PagedNetworkCachedResource<RepoPage>() {

        override fun loadNextPageTrigger(): LiveData<Boolean>? {
            return nextPageTrigger
        }

        override fun loadFromDb(page: Int): LiveData<RepoPage> {
            return repoDao.load(page)
        }

        override fun shouldFetch(data: RepoPage?): Boolean {
            return data == null || data.items.isEmpty()
        }

        override fun createCall(page: Int): LiveData<ApiResponse<ResponseBody>> {
            return appService.githubApi.getRepos("google", page)
        }

        override fun processResponse(response: ApiSuccessResponse<ResponseBody>): RepoPage {
            return RepoPage(
                response.currentPage,
                Repo.parseResponseBody(response.body)
            )
        }

        override fun saveCallResult(item: RepoPage) {
            repoDao.save(item)
        }

    }.asLiveData()

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: RepoRepository? = null

        fun getInstance(
            appService: AppService,
            repoDao: RepoDao
        ) =
            instance
                ?: synchronized(this) {
                instance
                    ?: RepoRepository(
                        appService,
                        repoDao
                    )
                        .also { instance = it }
            }
    }
}