package com.gitlab.rjga94.example.resourcelivedata.base

import androidx.fragment.app.Fragment

abstract class BaseFragment: Fragment() {
    protected val TAG = this::class.java.simpleName
}