package com.gitlab.rjga94.example.resourcelivedata.viewmodels.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gitlab.rjga94.example.resourcelivedata.data.repositories.RepoRepository
import com.gitlab.rjga94.example.resourcelivedata.screens.UserProfileFragmentArgs
import com.gitlab.rjga94.example.resourcelivedata.viewmodels.UserProfileViewModel

@Suppress("UNCHECKED_CAST")
class UserProfileViewModelFactory(
    private val args: UserProfileFragmentArgs,
    private val repoRepository: RepoRepository
): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UserProfileViewModel(
            args,
            repoRepository
        ) as T
    }
}