package com.gitlab.rjga94.example.resourcelivedata.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.rjga94.example.resourcelivedata.R
import com.gitlab.rjga94.example.resourcelivedata.data.models.Repo
import kotlinx.android.synthetic.main.rvi_repo.view.*

class ReposListAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var adapterData = mutableListOf<Repo>()

    fun addData(data: List<Repo>) {
        val rangeStart = adapterData.size
        val rangeEnd = rangeStart + data.lastIndex
        this.adapterData.addAll(data)
        notifyItemRangeInserted(rangeStart, rangeEnd)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return RepoVH(
            LayoutInflater.from(parent.context).inflate(R.layout.rvi_repo, parent, false)
        )
    }

    override fun getItemCount() = adapterData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val repo = adapterData[position]
        holder.itemView.apply {
            textViewTitle.text = repo.fullName
            textViewDescription.text = repo.description
        }
    }

    class RepoVH(v: View): RecyclerView.ViewHolder(v)
}