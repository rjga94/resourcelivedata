package com.gitlab.rjga94.example.resourcelivedata.utilities

import android.content.Context
import com.gitlab.rjga94.example.resourcelivedata.data.AppDatabase
import com.gitlab.rjga94.example.resourcelivedata.data.repositories.RepoRepository
import com.gitlab.rjga94.example.resourcelivedata.network.AppService
import com.gitlab.rjga94.example.resourcelivedata.screens.UserProfileFragmentArgs
import com.gitlab.rjga94.example.resourcelivedata.viewmodels.factories.UserProfileViewModelFactory

object InjectorUtils {
    fun provideUserProfileViewModelFactory(context: Context, args: UserProfileFragmentArgs) = UserProfileViewModelFactory(
            args,
            getRepoRepository(context)
    )

    private fun getRepoRepository(context: Context) = RepoRepository.getInstance(
        getAppService(context),
        AppDatabase.getInstance(context).repoDao()
    )

    private fun getAppService(context: Context) = AppService.getInstance()
}