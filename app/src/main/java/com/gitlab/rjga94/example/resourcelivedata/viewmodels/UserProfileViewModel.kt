package com.gitlab.rjga94.example.resourcelivedata.viewmodels

import androidx.lifecycle.ViewModel
import com.gitlab.rjga94.example.resourcelivedata.data.repositories.RepoRepository
import com.gitlab.rjga94.example.resourcelivedata.screens.UserProfileFragmentArgs

class UserProfileViewModel(
    private val args: UserProfileFragmentArgs,
    private val repoRepository: RepoRepository
): ViewModel() {
    val userUid: String = args.userUid

    val repos = repoRepository.getPaged()

    val reposNetworkOnly = repoRepository.getNetworkOnly("google")

    val reposPagedCachedOnly = repoRepository.getPagedCachedOnly()

    val reposPagedNetworkOnly = repoRepository.getPagedNetworkOnly("google")

    fun triggerLoadNextPage() {
        repoRepository.triggerLoadNextPage()
    }
}