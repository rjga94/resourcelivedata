package com.gitlab.rjga94.example.resourcelivedata.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.gitlab.rjga94.example.resourcelivedata.data.models.Repo

@Entity(tableName = "table_repo_page")
class RepoPage(
    @PrimaryKey
    val page: Int,
    val items: List<Repo>
)