package com.gitlab.rjga94.example.resourcelivedata.utilities

import androidx.room.TypeConverter
import com.gitlab.rjga94.example.resourcelivedata.data.models.Repo
import org.json.JSONArray

class DatabaseConverters {

    @TypeConverter
    fun jsonToRepoList(json: String?): List<Repo>? {
        if (json == null) return null

        val list = mutableListOf<Repo>()
        val objArray = JSONArray(json)
        (0 until objArray.length()).forEach {
            val obj = objArray.getJSONObject(it)
            list.add(
                Repo.parseJson(
                    obj
                )
            )
        }

        return list
    }

    @TypeConverter
    fun repoListToJson(list: List<Repo>?): String? {
        if (list == null) return null

        val objArray = JSONArray()
        list.forEach {
            objArray.put(it.toJson())
        }
        return objArray.toString()
    }
}